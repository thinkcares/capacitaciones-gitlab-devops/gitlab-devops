---
title: "Temas a tratar para la capacitación de DevOps"
---

# Gitlab

* Snipets
* Integraciones con otras herramientas 
* Socios
* Gitlab es abrumadoramente rico en funcionalidades
* Compañía Open Core
* Búsqueda
* Características
* Markdown
* Ejemplos de flujo de trabajo con GitLab (Milestones, issues, kanvan)
* Gitlab Pages
* Runners
* GitLab sustituye múltiples herramientas que antes se utilizaban
* Issues
* Web IDE
* integración con teams
* Ejemplo de ciclo de vida con GitLab
* Tabla de madurez de características
* Aplicación móvil

## Issues

* Qué es un issue
* Migrar issues a otros proyectos
* Issue templates

# Unix-Like y la supremacía de Linux en los servidores.


* Linux y sus modularidad. ¿un problema?
* Diferencias principales entre windows y linux
* Motivos para trabajar con linux
* Explicación de la filosofía de las distribuciones.
* Uso básico de terminales tipo unix

# Git control de versiones

* Origen de GIT
* configuración básica
* Básicos de git
* Relación entre las estrategias de versionamiento y DevOps
* SSH y GPG

# Contenedores

* Origen de los contenedores
* Analogía de los contenedores
* Construcción de imágenes
* Construcción de imágenes en varias etapas (multi Staging)




# DevOps y microservicios

* Mostrar gráfica para representar cómo es el ciclo de vida de los microservicios en comparación con un ciclo de vida monolítico.
* Orquestadores (Kubernetes, Rancher, Openshift, )
* Cómo mantener un Dockerfile
* Escalabilidad horizontal vs escalabilidad vertical
* Los principios de programación de aplicaciones modernas

# Miscelánea

* WSL y uso
 - Remote WSL para programar
* MarkDown
